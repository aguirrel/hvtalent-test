#ifndef __LIST_H__
#define __LIST_H__

typedef struct _Node {
        struct _Node *next;
            int numero;
} Node;

typedef struct _List {
        Node *first;
} List;

int List_empty(List *);
int List_count(List *);
int List_get(List *, int index);
int List_first(List *);
int List_last(List *);
void List_push(List *, int);
int List_pop(List *);
int List_remove_all(List *);
void List_apply(List *, void (*f)(int));
int List_find(List *, int);
int List_sum(List *);


#endif
