#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <list.h>

#define MAXN 10

void Check_empty(List *);
void Check_with_values(List *);
void Print_int(int);

int main()
{
    List *list;

    if((list = (List *)malloc(sizeof(List))) == NULL)
    {
        printf("Error al crear la lista\n");
        return -1;
    }

    list->first = NULL;
   
    printf("Chequeo las operaciones con la lista vacia: ");
    Check_empty(list);
    printf("OK\n");

    printf("Chequeo las operaciones con la lista con valores: ");
    Check_with_values(list);
    printf("OK\n");

    printf("Chequeo las operaciones con la lista vacia nuevamente: ");
    Check_empty(list);
    printf("OK\n");

    free(list);
    return 0;
}

void Check_empty(List *list)
{
    assert(List_empty(list) == 1);
    assert(List_count(list) == 0);
    assert(List_get(list, 1) == -1);
    assert(List_first(list) == -1);
    assert(List_last(list) == -1);
    assert(List_pop(list) == -1);
    assert(List_find(list, 1) == 0);
    assert(List_sum(list) == 0);
}

void Check_with_values(List *list)
{
    int numeros[MAXN] = {11,22,33,44,55,66,77,88,99,1010};
    int i;

    // Lleno la lista y prueba que cada numero agregado sea el primero en la lista
    for(i=0; i<MAXN; i++)
    {
        List_push(list, numeros[i]);
        assert(List_first(list) == numeros[i]);
    }
    
    assert(List_empty(list) == 0);
    assert(List_count(list) == MAXN);

    List_apply(list, Print_int);
    
    assert(List_sum(list) == 1505);

    assert(List_get(list, 3) == 77);
    assert(List_get(list, 2) == 88);
    assert(List_get(list, 10) == -1);

    assert(List_last(list) == 11);

    assert(List_first(list) == 1010);
    assert(List_pop(list) == 1010);
    assert(List_first(list) == 99);

    assert(List_last(list) == 11);

    assert(List_find(list, 11) == 1);
    assert(List_find(list, 1) == 0);

    // Limpio la lista
    List_remove_all(list);
}

void Print_int(int numero)
{
    printf("%d ", numero);
}

