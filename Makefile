CC=gcc
CFLAGS=-I.
DEPS = list.h
OBJ = app.o list.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

app: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f *.o *~ core 
