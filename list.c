#include <stdio.h>
#include <stdlib.h>
#include <list.h>

// Retorna 1 si la lista esta vacia, 0 sino
int List_empty(List *l)
{
    return (l->first == NULL ? 1 : 0);
}

// Retorna la cantidad elementos que tiene la lista
int List_count(List *l)
{
    Node *p;
    int length;

    for(length = 0, p = l->first; p != NULL; p = p->next, length++);

    return length;
}

// Obtiene un valor arbitrario de la lista
// *** Que se devuelve en caso de no existir el valor? ****
// *** Considero que solo guardo enteros positivos y en caso de error devuelve -1 ***
int List_get(List *l, int index)
{
    Node *p;
    int i;

    for(i=0, p = l->first; p != NULL && i != index; p = p->next, i++);

    return (p != NULL ? p->numero : -1);
}

// Retorna el primer valor de la lista
// IDEM List_get retorno de error
int List_first(List *l)
{
    return (l->first != NULL ? l->first->numero : -1);
}

// Retorna el ultimo valor de la lista
int List_last(List *l)
{
    Node *p;

    if (l->first == NULL) return -1;

    for(p = l->first; p->next != NULL; p = p->next);

    return p->numero;
}

// Agrega un número al principio de la lista
void List_push(List *l, int numero)
{
    Node *p;

    if((p = (Node *)malloc(sizeof(Node))) == NULL) return;

    p->numero = numero;
    p->next = l->first;
    l->first = p;

}

// Elimina el primer elemento de la lista, retorna el número que eliminó
int List_pop(List *l)
{
    Node *p;
    int numero;

    if(l->first == NULL) return -1;

    p = l->first;
    l->first = p->next;
    numero = p->numero;
    free(p);

    return numero;
}

// Elimina todos los elementos de la lista
// *** Debería ser void ya que no se necesita retornar nada ***
int List_remove_all(List *l)
{
    Node *p;

    for(p = l->first; l->first != NULL; p = l->first)
    {
        l->first = p->next;
        free(p);
    }

    return 0;
}

// Aplica la función f a todos los números de la lista
void List_apply(List *l, void (*f)(int))
{
    Node *p;

    for(p = l->first; p != NULL; p = p->next)
    {
        f(p->numero);
    }
}

// Retorna 1 si el número pasado por parámetro esta en la lista, 0 sino
int List_find(List *l, int numero)
{
    Node *p;

    for(p = l->first; p != NULL && p->numero != numero; p = p->next);

    return (p != NULL ? 1 : 0);
}

// Retorna la sumatoria de todos los elementos de la lista
int List_sum(List *l)
{
    Node *p;
    int sum;
    
    for(sum = 0, p = l->first; p != NULL; sum += p->numero, p = p->next);

    return sum;
}

